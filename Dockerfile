# Run test-kitchen with Vagrant/VirtualBox

ARG RUBY_VERSION=3.0.6-bullseye
FROM ruby:$RUBY_VERSION

ENV DEBIAN_FRONTEND noninteractive

VOLUME /dev/vboxdrv:/dev/vboxdrv:rw

RUN set -x \
    && wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | gpg --dearmor --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg \
    && echo "deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian bullseye contrib" > /etc/apt/sources.list.d/virtualbox.list \
    && echo virtualbox-ext-pack virtualbox-ext-pack/license select true | debconf-set-selections \
    && apt-get update

RUN apt-get install -y --no-install-recommends ca-certificates curl jq \
    virtualbox-7.0 kmod \
    build-essential git gnupg zlib1g-dev libreadline-dev libssl-dev \
    libcurl4-openssl-dev ruby-full lsb-release

RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor > /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    && echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list \
    && apt-get update && apt-get install -y vagrant \
    && apt-get clean all

ADD files /root
WORKDIR /root

RUN vagrant plugin install vagrant-cachier \
    && vagrant plugin install vagrant-vbguest

RUN ln -s 'VirtualBoxVMs' 'VirtualBox VMs'
