# Docker image for test-kitchen + Vagrant + Virtualbox

The purpose of this Docker image is to run [test-kitchen]() with [Vagrant]() and
[Virtualbox]() on [Gitlab CI]().

## Installation

Set up the Gitlab Runner using the `install-runner.sh` script.
