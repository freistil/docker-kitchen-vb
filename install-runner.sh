#!/bin/bash

if [ -z "$GITLAB_TOKEN" ]; then
  echo "Please `export GITLAB_TOKEN=<your runner token>`!"
  exit 1
fi

gitlab-runner register -n -u https://gitlab.com -r $GITLAB_TOKEN \
  --name test-kitchen --tag-list test-kitchen --executor docker --limit 1 \
  --docker-image registry.gitlab.com/freistil/docker-kitchen-vb:latest --docker-privileged \
  --docker-volumes '/var/lib/gitlab-runner/test-kitchen/vagrant-boxes:/root/.vagrant.d/boxes:rw' \
  --docker-volumes '/var/lib/gitlab-runner/test-kitchen/virtualbox-vms:/root/VirtualBox VMs:rw' \
  --docker-volumes '/var/lib/gitlab-runner/test-kitchen/virtualbox-config:/root/.config/VirtualBox:rw' \
  --docker-volumes '/var/lib/gitlab-runner/test-kitchen/vagrant-cachier:/root/.vagrant.d/cache:rw' \
  --docker-volumes '/var/lib/gitlab-runner/test-kitchen/kitchen-cache:/root/.kitchen/cache:rw' \
  --docker-allowed-services '' \
  --docker-allowed-images registry.gitlab.com/freistil/docker-kitchen-vb:latest
